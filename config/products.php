<?php
return [
        'products_list' => [
            [
                'name' => 'Samsung Galaxy',
                'slug' => 'product-1',
                'image' => 'https://picsum.photos/200/300',
                'price'=> 150
            ],
            [
                'name' => 'Apple iPhone 12',
                'slug' => 'product-2',
                'image' => 'https://picsum.photos/200/300',
                'price' => 180
            ],
            [
                'name' => 'Google Pixel 2 XL',
                'slug' => 'product-3',
                'image' => 'https://picsum.photos/200/300',
                'price' => 200
            ],
            [
                'name' => 'LG V10 H800',
                'slug' => 'product-4',
                'image' => 'https://picsum.photos/200/300',
                'price' => 220
            ],
        ]
    ];
