<?php namespace App\Modules\Services\Product;

use App\Modules\Models\Banner\Banner;
use App\Modules\Models\Product\Product;
use App\Modules\Services\Service;

class ProductService extends Service
{
    public function __construct()
    {
        //Product model can be injected here if database is added later
    }

    /**
     * Paginate all Session
     *
     * @param array $filter
     * @return void
     */
    public function paginate(array $filter = [])
    {

    }

    /**
     * Get all Session
     *
     * @return \Illuminate\Config\Repository|\Illuminate\Contracts\Foundation\Application|mixed
     */
    public function all()
    {
        $products = config('products.products_list');
        return collect($products)->map(function ($product) {
            return (object) $product;
        });


    }

    /**
     * Get a Session
     *
     * @param $productId
     * @return void
     */
    public function find($productId)
    {

    }


    /**
     * write brief description
     * @param $name
     * @return mixed
     */

    public function getBySlug($slug){
        $products = $this->all();
       return   $products->filter(function($value) use ($slug){
            return $value->slug == $slug;
        })->first();

    }

}
