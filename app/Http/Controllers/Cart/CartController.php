<?php

namespace App\Http\Controllers\Cart;

use App\Http\Controllers\Controller;
use App\Modules\Services\Product\ProductService;
use Illuminate\Http\Request;

class CartController extends Controller
{
    protected $product;
    function __construct(ProductService $product)
    {
        $this->product = $product;
    }

   function addToCart($slug)
   {

      $selectedProduct = $this->product->getBySlug($slug);
      $productSlug = array('slug'=>$selectedProduct->slug,'qty'=>1);

      if (session()->has('cart_products')) {
          $savedCartProducts = session()->get('cart_products');
          $savedCartProducts = collect($savedCartProducts)->map(function ($savedCartProduct) {
              return (object)$savedCartProduct;
          });

          if (count($savedCartProducts) > 0) {

              $temp = [];
              $productExist = (bool)$savedCartProducts->where('slug', $slug)->first();
              if(!$productExist)
              {
                  array_push($temp,$productSlug);
              }
              foreach ($savedCartProducts as $key=>$cartProduct) {
                  $qp =  array('slug'=>$cartProduct->slug,'qty'=>1);
                  array_push($temp,$qp);
              }

              session()->put('cart_products', $temp);

          }
      }else
      {
          $temp = [];
          array_push($temp,$productSlug);
          session()->put('cart_products',$temp);
      }


      return back()->with('success','Product added to cart');
   }


   function cart()
   {
       $cartProducts = [];
       $carts = session()->get('cart_products');
       if(!empty($carts)) {
           foreach ($carts as $cart){
               $product = $this->product->getBySlug($cart['slug']);
               $product->qty = $cart['qty'];
               array_push($cartProducts,$product);
           }
           return view('cart.index',compact('cartProducts'));
       }

   }


    public function remove(Request $request)
    {
        $slug = $request->input('slug');

        if ($slug) {
            $cart = session()->get('cart_products');
            $cart = collect($cart)->reject(function ($value) use ($slug) {
                return $value['slug'] == $slug;
            });
            session()->put('cart_products', $cart);
            session()->flash('success', 'Product removed successfully');
        }
    }


    public function update(Request $request)
    {
        $slug = $request->input('slug');
        $quantity = $request->input('quantity');

        if($slug && $quantity){
            $cart = session()->get('cart_products');
            $cart = collect($cart)->map(function ($value) use ($slug, $quantity) {
               if ($value['slug'] == $slug) {
                   $value['qty'] = $quantity;
               }
               return $value;
            });
            session()->put('cart_products', $cart);
            session()->flash('success', 'Cart updated successfully');
        }
    }

}
