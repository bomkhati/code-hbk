@extends('layout')

@section('content')

    <div class="container-fluid">
        @include('layouts.flash-message')
        <div class="row products">

            <table id="cart" class="table table-hover table-condensed">
                <thead>
                <tr>
                    <th style="width:50%">Product</th>
                    <th style="width:10%">Price</th>
                    <th style="width:8%">Quantity</th>
                    <th style="width:22%" class="text-center">Subtotal</th>
                    <th style="width:10%"></th>
                </tr>
                </thead>
                <tbody>
                @php $total = 0 @endphp
                    @foreach($cartProducts as $cart)
                        @php $total += $cart->price * $cart->qty; @endphp
                            <tr data-slug="{{ $cart->slug }}">
                                <td data-th="Product">
                                    <div class="row">
                                        <div class="col-sm-3 hidden-xs">
                                            <img src="{{ $cart->image }}" width="100" height="100" class="img-responsive"/></div>
                                        <div class="col-sm-9">
                                            <h6 class="nomargin" class="text-center" style="padding-top: 2rem">{{ $cart->name }}</h6>
                                        </div>
                                    </div>
                                </td>
                                <td data-th="Price">${{ $cart->price }}</td>
                                <td data-th="Quantity">
                                    <input type="number" value="{{ $cart->qty }}" class="form-control quantity update-cart" />
                                </td>
                                <td data-th="Subtotal" class="text-center">${{ $cart->price * $cart->qty }}</td>
                                <td class="actions" data-th="">
                                    <div class="btn btn-danger btn-sm remove-from-cart">Remove</div>
                                </td>
                            </tr>
                    @endforeach
                </tbody>

                <tfoot>
                <tr>
                    <td colspan="5" class="text-right"><h3><strong>Total: ${{ $total }}</strong></h3></td>
                </tr>
                <tr>
                    <td colspan="5" class="text-right">
                        <a href="{{ url('/') }}" class="btn btn-warning"><i class="fa fa-angle-left"></i> Continue Shopping</a>
                        <button class="btn btn-success">Checkout</button>
                    </td>
                </tr>
                </tfoot>

            </table>
        </div>
    </div>
@endsection

@section('page-specific-scripts')
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
    <script type="text/javascript">

        $(".remove-from-cart").click(function (e) {
            e.preventDefault();
            var ele = $(this);

            if(confirm("Are you sure want to remove?")) {
                $.ajax({
                    url: '{{ route('remove.from.cart') }}',
                    method: "POST",
                    data: {
                        _token: '{{ csrf_token() }}',
                        slug: ele.parents("tr").attr("data-slug")
                    },
                    success: function (response) {
                        window.location.reload();
                    }
                });
            }
        });

        $(".update-cart").change(function (e) {
            e.preventDefault();
            var ele = $(this);
            $.ajax({
                url: '{{ route('update.cart') }}',
                method: "POST",
                data: {
                    _token: '{{ csrf_token() }}',
                    slug: ele.parents("tr").attr("data-slug"),
                    quantity: ele.parents("tr").find(".quantity").val()
                },
                success: function (response) {
                    window.location.reload();
                }
            });
        });

    </script>
@endsection
