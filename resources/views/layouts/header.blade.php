<nav class="navbar navbar-expand-sm bg-dark navbar-dark">
    <div class="col-md-4">   <a class="navbar-brand" href="#">Logo</a></div>
        <div class="col-md-8 header-right">
            <a href="{{ route('cart') }}">
                <div class="btn btn-info">
                    <i class="fa fa-shopping-cart" aria-hidden="true"></i> Cart <span class="badge badge-pill badge-danger">{{ session()->has('cart_products') ? array_sum(collect(session('cart_products'))->pluck('qty')->toArray()) : 0 }}</span>
                </div>
            </a>
        </div>
</nav>
