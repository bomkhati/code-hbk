
@include('layouts.head')
@include('layouts.header')
@yield('content')
@yield('page-specific-scripts')
@include('layouts.footer')


