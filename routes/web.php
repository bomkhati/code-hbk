<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [\App\Http\Controllers\Product\ProductController::class, 'index'])->name('home');
Route::get('product/{slug}/add-to-cart', [\App\Http\Controllers\Cart\CartController::class, 'addToCart'])->name('add-to-cart');
Route::get('cart', [\App\Http\Controllers\Cart\CartController::class, 'cart'])->name('cart');
Route::post('update-cart', [\App\Http\Controllers\Cart\CartController::class, 'update'])->name('update.cart');
Route::post('remove-from-cart', [\App\Http\Controllers\Cart\CartController::class, 'remove'])->name('remove.from.cart');
